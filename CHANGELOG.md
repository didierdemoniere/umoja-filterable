# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="8.0.0"></a>
# [8.0.0](https://bitbucket.org/didierdemoniere/umoja-filterable/compare/v7.0.0...v8.0.0) (2021-06-06)


### Performance Improvements

* **create function:** remove preserveContext ([fb2726b](https://bitbucket.org/didierdemoniere/umoja-filterable/commits/fb2726b))


### BREAKING CHANGES

* **create function:** preserveContext option removed from create function



<a name="7.0.0"></a>
# [7.0.0](https://bitbucket.org/didierdemoniere/umoja-filterable/compare/v6.0.1...v7.0.0) (2021-05-27)


### Code Refactoring

* **all + typings:** simplify all function and types ([c33d254](https://bitbucket.org/didierdemoniere/umoja-filterable/commits/c33d254))


### BREAKING CHANGES

* **all + typings:** all now takes only an extra clone parameter and all types are simplified



<a name="6.0.1"></a>
## [6.0.1](https://bitbucket.org/didierdemoniere/umoja-filterable/compare/v6.0.0...v6.0.1) (2021-05-25)


### Bug Fixes

* **filterables:** fix Filterables typing ([1c6c6a3](https://bitbucket.org/didierdemoniere/umoja-filterable/commits/1c6c6a3))



<a name="6.0.0"></a>
# [6.0.0](https://bitbucket.org/didierdemoniere/umoja-filterable/compare/v5.0.0...v6.0.0) (2021-03-23)


### Features

* **all:** add options to turn class instance into object with binded functions ([cd3ac13](https://bitbucket.org/didierdemoniere/umoja-filterable/commits/cd3ac13))


### BREAKING CHANGES

* **all:** change "all" signature to accept an option object



<a name="5.0.0"></a>
# [5.0.0](https://bitbucket.org/didierdemoniere/umoja-filterable/compare/v4.4.1...v5.0.0) (2021-01-10)


### Features

* **general:** aPI change, bug fix on all, add option on all, documentation ([1de1800](https://bitbucket.org/didierdemoniere/umoja-filterable/commits/1de1800))


### BREAKING CHANGES

* **general:** parameter 'nocontext' with false as default is now 'preserveContext' with true as
default



<a name="4.4.1"></a>
## [4.4.1](https://bitbucket.org/didierdemoniere/umoja-filterable/compare/v4.4.0...v4.4.1) (2021-01-03)



<a name="4.4.0"></a>
# [4.4.0](https://bitbucket.org/didierdemoniere/umoja-filterable/compare/v4.3.1...v4.4.0) (2021-01-03)


### Features

* **all:** add class instance support ([593ecde](https://bitbucket.org/didierdemoniere/umoja-filterable/commits/593ecde))



<a name="4.3.1"></a>
## [4.3.1](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/filterable/compare/v4.3.0...v4.3.1) (2020-02-03)



<a name="4.3.0"></a>
# [4.3.0](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/filterable/compare/v4.2.0...v4.3.0) (2020-01-25)


### Features

* **all:** add nocontext option to the function all ([e105936](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/filterable/commit/e105936))



<a name="4.2.0"></a>
# [4.2.0](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/filterable/compare/v4.1.1...v4.2.0) (2020-01-25)


### Features

* **general:** add no-context option ([13e1058](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/filterable/commit/13e1058))



<a name="4.1.1"></a>
## [4.1.1](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/filterable/compare/v4.1.0...v4.1.1) (2019-07-15)


### Bug Fixes

* **all:** not modify source object ([a56fa9e](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/filterable/commit/a56fa9e))



<a name="4.1.0"></a>
# [4.1.0](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/filterable/compare/v4.0.0...v4.1.0) (2019-07-15)


### Features

* **all:** add helper to make filterable all method of an object ([5061ccd](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/filterable/commit/5061ccd))



<a name="4.0.0"></a>
# [4.0.0](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/filterable/compare/v3.2.1...v4.0.0) (2019-07-10)


### ci

* **general:** add configuration for branch production package ([41e944b](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/filterable/commit/41e944b))


### BREAKING CHANGES

* **general:** this source package can no longer be installed directly



<a name="3.2.1"></a>
## [3.2.1](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/filterable/compare/v3.2.0...v3.2.1) (2019-05-20)



<a name="3.2.0"></a>
# [3.2.0](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/filterable/compare/v3.1.2...v3.2.0) (2019-02-04)


### Features

* **filters:** preserve execution context in filters and implementation ([9ff8aeb](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/filterable/commit/9ff8aeb))



<a name="3.1.2"></a>
## [3.1.2](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/filterable/compare/v3.1.1...v3.1.2) (2018-11-29)


### Bug Fixes

* **fix:** add arity to compile chain ([20cfc0e](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/filterable/commit/20cfc0e))



<a name="3.1.1"></a>
## [3.1.1](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/filterable/compare/v3.1.0...v3.1.1) (2018-10-20)



<a name="3.1.0"></a>
# [3.1.0](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/filterable/compare/v3.0.1...v3.1.0) (2018-10-20)


### Features

* **all:** add arity option ([6f08e2f](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/filterable/commit/6f08e2f))



<a name="3.0.1"></a>
## [3.0.1](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/filterable/compare/v3.0.0...v3.0.1) (2018-10-19)



<a name="3.0.0"></a>
# [3.0.0](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/filterable/compare/v2.0.1...v3.0.0) (2018-10-19)


### Features

* **all:** filterables are now functions with extra prroperties ([8c5ce04](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/filterable/commit/8c5ce04))


### BREAKING CHANGES

* **all:** run property removed



<a name="2.0.1"></a>
## [2.0.1](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/filterable/compare/v2.0.0...v2.0.1) (2018-10-06)


### Bug Fixes

* **applyFilters:** filter was applied in wrong order ([c74fe6d](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/filterable/commit/c74fe6d))



<a name="2.0.0"></a>
# 2.0.0 (2018-08-05)


### Code Refactoring

* **apply, unapply, clear:** api change to avoid confusion with Function.apply ([d713f8a](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/filterable/commit/d713f8a))


### Features

* **all:** first version ([7542f5f](https://github.com/YOUR_GITHUB_USER_NAME/@umoja/filterable/commit/7542f5f))


### BREAKING CHANGES

* **apply, unapply, clear:** apply, unapply, clear
