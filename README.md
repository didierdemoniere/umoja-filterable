# @umoja/filterable

## simple aspect oriented programming

Filters wrap around each other and then finally around the implementation. While the first added filter is called with the input first, and last in receiving the result.

Inspired by [li3](http://li3.me) [filtering system](http://li3.me/docs/api/lithium/1.1.x/lithium/aop/Chain).


Features:
* small API surface 3 functions only: (applyFilter, unapplyFilter, clearFilters)
* functional
* composition based
* async filtering support (promise)
* quite simple
* small
* 1 minute learning curve
* typed (typescript)


### Example
#### API.js
```js
import { all as filterableAll } from '@umoja/filterable'
// or const filterableAll = require('@umoja/filterable').all

const API = filterableAll({
    create (name, id) {
        return {id, text: `${name} ${id}`}
    }
})

export { ...API }
// or  module.exports = { ...API }
```

#### Post.js
```js
import { create as createFilterable } from '@umoja/filterable'
// or const createFilterable = require('@umoja/filterable').create
import * as API from './API'


class Post {
    public static create = createFilterable(function (filtered_id) {
        return API.create('Post', filtered_id)
    })
}

export { Post }
// or  module.exports = { Post }
```

#### index.js
```js
import * as API from './API'
import { Post } from './Post'

API.create.applyFilter((next, name, id) => {
    console.log(`API before create: ${name} ${id}`)
    const result = next(name, id);
    console.log(`API after create: ${name} ${id}`)
    return result
});

Post.create.applyFilter((next, id) => {
    console.log(`Post before create: ${id}`)
    const result = next(id);
    console.log(`Post after create: ${id}`)
    return result
});


Post.create(1)
// log: Post before create: 1
// log: API before create: Post 1
// log: API after create: Post 1
// log: Post after create: 1
// => {id: 1, text: `Post 1`}
```
