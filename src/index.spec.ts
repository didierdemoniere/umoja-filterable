// tslint:disable:no-expression-statement only-arrow-functions max-classes-per-file

import test from 'ava';
import { all, create as createFilterable } from './';

test('filterable without filter must operate as not filterable', t => {
  const double = createFilterable((val: number) => val * 2, 1);

  t.is(double(2), 4);
});

test('filterable with filters must behave accordingly', t => {
  const double = createFilterable((val: number) => val * 2);
  double.applyFilter((next, n) => next(n - 1));

  t.is(double(2), 2);
});

test('once filters removed implementation works as expected', t => {
  const double = createFilterable((val: number) => val * 2);

  const unapplyFilter = double.applyFilter((next, n) => next(n - 1));

  t.is(double(2), 2);
  t.is(unapplyFilter(), true);
  // filter already removed so operation failed
  t.is(unapplyFilter(), false);
  t.is(double(2), 4);
});

test('filters can be removed all at once', t => {
  const double = createFilterable((val: number) => val * 2);

  double.applyFilter((next, n) => next(n - 1));
  double.applyFilter((next, n) => next(n - 1));

  t.is(double(2), 0);
  t.is(double.clearFilters(), true);
  t.is(double(2), 4);
});

test('simple case with 2 filters', t => {
  const filterable = createFilterable((list: string[]) => {
    list.push('third');
    t.deepEqual(list, ['first', 'second', 'third']);
  });

  filterable.applyFilter((next, list) => {
    list.push('second');
    return next(list);
  });

  filterable.applyFilter((next, list) => {
    list.push('first');
    return next(list);
  });

  filterable([]);
});

test('ensure 5 arguments filters working', t => {
  const sum = createFilterable(
    (a1: number, a2: number, a3: number, a4: number, a5: number) =>
      a1 + a2 + a3 + a4 + a5
  );

  sum.applyFilter((next, a1, a2, a3, a4, a5) =>
    next(a1 - 1, a2 - 1, a3 - 1, a4 - 1, a5 - 1)
  );

  t.is(sum(2, 2, 2, 2, 2), 5);
  t.is(sum.clearFilters(), true);
  t.is(sum(2, 2, 2, 2, 2), 10);
});

test('all', t => {
  let mem: number = 0;
  const getset = all({
    get: () => mem,
    set: (value: number) => {
      mem = value;
    }
  });

  t.is(typeof getset.get, 'function');
  t.is(typeof getset.get.applyFilter, 'function');
  t.is(typeof getset.get.unapplyFilter, 'function');
  t.is(typeof getset.get.clearFilters, 'function');
  t.is(typeof getset.set, 'function');
  t.is(typeof getset.set.applyFilter, 'function');
  t.is(typeof getset.set.unapplyFilter, 'function');
  t.is(typeof getset.set.clearFilters, 'function');

  t.is(getset.get(), 0);
  getset.set(1);
  t.is(getset.get(), 1);
  getset.set(0);
  getset.set.applyFilter((next, val) => next(val + 1));
  getset.set(1);
  t.is(getset.get(), 2);
});

test('option no-context', t => {
  const double = createFilterable(function(val: number) {
    t.is(this, undefined);
    return val * 2;
  }, 1);

  double.applyFilter(function(next, n) {
    t.is(this, undefined);
    return next(n - 1);
  });

  t.is(double.call({ imacontext: true }, 2), 2);
});

test('class instance support', t => {
  class Greeter {
    public greeting: string;

    constructor(message: string) {
      this.greeting = message;
    }

    public greet(name: string) {
      return 'Hello ' + name + ', ' + this.greeting;
    }
  }

  const originalGreeter = new Greeter('welcome');
  const greeter = all(originalGreeter);

  greeter.greet.applyFilter((next, name) => `${next(name)} !!!`);

  t.not(greeter, originalGreeter);
  t.is(greeter.greeting, 'welcome');
  t.is(greeter.greet('john'), 'Hello john, welcome !!!');
});

test('class instance support - feature no clone', t => {
  class Greeter {
    public greeting: string;

    constructor(message: string) {
      this.greeting = message;
    }

    public greet(name: string) {
      return 'Hello ' + name + ', ' + this.greeting;
    }
  }

  const originalGreeter = new Greeter('welcome');
  const greeter = all(originalGreeter, false);

  greeter.greet.applyFilter((next, name) => `${next(name)} !!!`);

  t.is(greeter, originalGreeter);
  t.is(greeter.greeting, 'welcome');
  t.is(greeter.greet('john'), 'Hello john, welcome !!!');
});

test('class instance support - bug spread', t => {
  class Greeter {
    public greeting: string;

    constructor(message: string) {
      this.greeting = message;
    }

    public greet(name: string): string;
    public greet(...args: string[]) {
      return 'Hello ' + args[0] + ', ' + this.greeting;
    }
  }

  const originalGreeter = new Greeter('welcome');
  const greeter = all(originalGreeter, false);

  greeter.greet.applyFilter((next, name) => `${next(name)} !!!`);

  t.is(greeter, originalGreeter);
  t.is(greeter.greeting, 'welcome');
  t.is(greeter.greet('john'), 'Hello john, welcome !!!');
});

test('class to functions - no context in filters', t => {
  class Greeter {
    public greeting: string;

    constructor(message: string) {
      this.greeting = message;
    }

    public greet(name: string) {
      return 'Hello ' + name + ', ' + this.greeting;
    }
  }

  const originalGreeter = new Greeter('welcome');
  const greeter = all(originalGreeter, false);

  const greet = greeter.greet;

  greet.applyFilter(function(next, name) {
    t.is(this, undefined);
    return `${next(name)} !!!`;
  });

  t.is(greeter, originalGreeter);
  t.is(greeter.greeting, 'welcome');

  t.is(greet('john'), 'Hello john, welcome !!!');
});
