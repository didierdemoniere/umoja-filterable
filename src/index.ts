export type Callable = (...args) => any;
type Filter = (next: Callable, ...args) => any;

/**
 * compile a list of filters and an implementation into a callable function
 *
 * @param chain list of filters
 * @param implementation original function
 * @param arity implementation number of arguments
 */
function compileCallChain<
  C extends Array<(next: Callable, ...args) => any>,
  I extends Callable
>(chain: C, implementation: I, arity: number) {
  if (chain.length === 0) {
    return implementation;
  }

  switch (arity) {
    case 0:
      return chain.reduce((next: Callable, filter: Filter) => {
        return () => filter(next);
      }, implementation);
    case 1:
      return chain.reduce((next: Callable, filter: Filter) => {
        return a1 => filter(next, a1);
      }, implementation);
    case 2:
      return chain.reduce((next: Callable, filter: Filter) => {
        return (a1, a2) => filter(next, a1, a2);
      }, implementation);
    case 3:
      return chain.reduce((next: Callable, filter: Filter) => {
        return (a1, a2, a3) => filter(next, a1, a2, a3);
      }, implementation);
    case 4:
      return chain.reduce((next: Callable, filter: Filter) => {
        return (a1, a2, a3, a4) => filter(next, a1, a2, a3, a4);
      }, implementation);
    default:
      return chain.reduce((next: Callable, filter: Filter) => {
        return (...args) => filter(next, ...args);
      }, implementation);
  }
}

export type Filterable<FN extends (...args: any) => any> = ((
  ...args: Parameters<FN>
) => ReturnType<FN>) & {
  clearFilters(): boolean;
  applyFilter(
    filter: (
      next: (...args: Parameters<FN>) => ReturnType<FN>,
      ...args: Parameters<FN>
    ) => ReturnType<FN>
  ): () => boolean;
  unapplyFilter(
    filter: (
      next: (...args: Parameters<FN>) => ReturnType<FN>,
      ...args: Parameters<FN>
    ) => ReturnType<FN>
  ): boolean;
};

/**
 * create a filterable clone of a function
 * @param implementation original function
 * @param arity implementation number of arguments
 */
export function create<FN extends (...args: any) => any>(
  implementation: FN,
  arity?: number
): Filterable<FN>;

export function create(implementation: (...args) => any, arity?: number) {
  arity = typeof arity === 'number' ? arity : implementation.length;
  const chain: Filter[] = [];
  let callChain = implementation;

  const run: any =
    arity === 0
      ? () => callChain()
      : arity === 1
      ? arg1 => callChain(arg1)
      : arity === 2
      ? (arg1, arg2) => callChain(arg1, arg2)
      : arity === 3
      ? (arg1, arg2, arg3) => callChain(arg1, arg2, arg3)
      : arity === 4
      ? (arg1, arg2, arg3, arg4) => callChain(arg1, arg2, arg3, arg4)
      : (...args) => callChain(...args);

  function applyFilter(filter: Filter) {
    chain.push(filter);
    callChain = compileCallChain(chain, implementation, arity);
    return () => unapplyFilter(filter);
  }

  function clearFilters() {
    const result = chain.length > 0 ? !(chain.length = 0) : false;
    callChain = implementation;
    return result;
  }

  function unapplyFilter(filter: Filter) {
    const id = chain.indexOf(filter);
    const result = id !== -1 ? !!chain.splice(id, 1) : false;
    if (result) {
      callChain = compileCallChain(chain, implementation, arity);
    }
    return result;
  }

  run.applyFilter = applyFilter;
  run.unapplyFilter = unapplyFilter;
  run.clearFilters = clearFilters;

  return run;
}

export interface Mapping {
  [key: string]: any;
}

// Represents all objects mapable with Maper E
export type Filterables<M extends Mapping> = M &
  {
    [P in keyof M]: M[P] extends (...args: any[]) => any
      ? Filterable<M[P]>
      : M[P];
  };

/**
 * create a shallow clone of an object where it's methods/functions are filterables
 *
 * @param obj source object
 * @param clone set to false to modify source object instead of a clone
 */
export function all<M extends Mapping>(
  obj: M,
  clone: boolean = true
): Filterables<M> {
  const methods =
    obj.constructor === Object
      ? Object.keys(obj).filter(
          name => typeof obj[name] === 'function' && !/^_/.test(name)
        )
      : getInstanceMethodNames(obj);

  return methods.reduce(
    (result, prop) => {
      result[prop] = create(
        obj[prop].bind(obj),
        obj[prop].length === 0 ? 5 : obj[prop].length
      );

      return result;
    },
    clone ? objectShallowClone(obj) : obj
  );
}

/**
 * get own method names of an instance
 *
 * @param obj source object
 */
function getInstanceMethodNames(obj) {
  const proto = Object.getPrototypeOf(obj);
  const names = Object.getOwnPropertyNames(proto);
  return names.filter(
    name => typeof obj[name] === 'function' && !/^_|^constructor$/.test(name)
  );
}

/**
 * create a shallow clone of an object
 * @param obj source object
 */
function objectShallowClone(obj: any) {
  return Object.assign(Object.create(Object.getPrototypeOf(obj)), obj);
}
